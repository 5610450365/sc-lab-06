package Two;

public abstract class Bird {
	private String name;
	
	public Bird(String name){
		this.name=name;
	}
	
	public abstract String Color();
	
	
	
	public String toString(){
		return this.name;
	}
	
	

}
